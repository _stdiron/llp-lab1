### Информация
Исходный код тестов представлен в *tests.c*. 
Примеры основных операций над базой данных, с использованием
пользовательского API, представлены в *main.c*.
### Сборка
```shell
~ cmake -Bcmake-debug-build -H.
~ cmake --build cmake-build-debug --target all
```